/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
* Copyright (C) 2019 Microchip Technology Inc. .
*
* Subject to your compliance with these terms, you may use Microchip software
* and any derivatives exclusively with Microchip products. It is your
* responsibility to comply with third party license terms applicable to your
* use of third party software (including open source software) that may
* accompany Microchip software.
*
* THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
* EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
* WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
* PARTICULAR PURPOSE.
*
* IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
* INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
* WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
* BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
* FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
* ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
* THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
*******************************************************************************/
//DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "sqi_task.h"

//uint32_t ReadCoreTimer( void )
//{
//    volatile uint32_t timer;
//    // get the count reg 
//    asm volatile("mfc0 %0, $9" : "=r"(timer));
//    return(timer);
//}


// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

int k = 0;
static volatile unsigned int result = 0;
static volatile float time_all = 0;
static volatile float time_write_enable = 0;
static volatile float time_erase = 0;
static volatile float time_write = 0;
static volatile float time_verify = 0;

int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );

    SQI_TASK_Initialize();

    printf("\n\r-------------------------------------------------------------");
    printf("\n\r\t\t DMAC Memory Transfer DEMO\t\t");
    printf("\n\r-------------------------------------------------------------");
    
    while ( true )
    {
        
        SQI_TASK_State_Machine();
       
        if ( finished_all )
        {
            LED1_On();
            finished_all = false;
            //started_all = false;
            
            // All Time:
            result = stop_all - start_all;
            time_all = (float) (result / 100e3); // ms
            
            // Erase Time:
            result = stop_erase - start_erase;
            time_erase = (float) (result / 100e3); // ms

            // Write Enable Time:
            result = stop_write_enable - start_write_enable;
            time_write_enable = (float) (result / 100e3); // ms   
            
            // Write Time:
            result = stop_write - start_write;
            time_write = (float) (result / 100e3); // ms            

            // Verify Time:
            result = stop_verify - start_verify;
            time_verify = (float) (result / 100e3); // ms 
            
            Nop(); 
        }
        LED1_Off();
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

