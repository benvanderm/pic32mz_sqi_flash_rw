/*******************************************************************************
 
  Author: 
    Bennie van der Merwe
 
  Date:
    October, 2021
 
   File Name:
    sqi_task.c

  Summary:
    - This file contains source code, modified from Microchip example code,  
    for the PIC32MZ2048EFM144's SQI module command interface. 
    - Communication is specifically with the Winbond W25N01GVxxxR Flash Device.
 
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "sqi_task.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the SQI_TASK_Initialize function.

    Application strings and buffers are be defined outside this structure.
*/

SQI_TASK_DATA CACHE_ALIGN sqi_taskData;

static uint32_t write_index = 0;
static uint32_t sector_index = 0;

sqi_dma_desc_t CACHE_ALIGN sqiCmdDesc[ CMD_DESC_NUMBER ]; // CMD_DESC_NUMBER == 5
sqi_dma_desc_t CACHE_ALIGN sqiBufDesc[ BUFF_DESC_NUMBER ]; // BUFF_DESC_NUMBER = Number of 2kByte pages in a 128kByte Block = 64


// Commands for the WINBOND W25N01GVxxxR Flash chip (64MByte):
uint8_t CACHE_ALIGN sqi_cmd_flash_reset;
uint8_t CACHE_ALIGN sqi_cmd_jedec_id_read[2] = { 0x9F, DUMMY_BYTE }; //, DUMMY_BYTE, DUMMY_BYTE, DUMMY_BYTE };
uint8_t CACHE_ALIGN sqi_cmd_read_status_register[2]; // Read Status Register
uint8_t CACHE_ALIGN sqi_cmd_write_status_register[3]; // Write Status Register
uint8_t CACHE_ALIGN sqi_cmd_write_enable;
uint8_t CACHE_ALIGN sqi_cmd_write_disable;
uint8_t CACHE_ALIGN sqi_cmd_block_erase[4];
uint8_t CACHE_ALIGN sqi_cmd_program_data_load[3];
uint8_t CACHE_ALIGN sqi_cmd_random_program_data_load[3];
uint8_t CACHE_ALIGN sqi_cmd_quad_program_data_load[3];
uint8_t CACHE_ALIGN sqi_cmd_random_quad_program_data_load[3];
uint8_t CACHE_ALIGN sqi_cmd_program_execute[4];       
uint8_t CACHE_ALIGN sqi_cmd_page_data_read[4];
uint8_t CACHE_ALIGN sqi_cmd_read[4];       
uint8_t CACHE_ALIGN sqi_cmd_fast_read[4];       
uint8_t CACHE_ALIGN sqi_cmd_fast_read_dual_output[4];
uint8_t CACHE_ALIGN sqi_cmd_fast_read_quad_output[4];
uint8_t CACHE_ALIGN sqi_cmd_fast_read_dual_io[4];
uint8_t CACHE_ALIGN sqi_cmd_fast_read_quad_io[5];
uint8_t CACHE_ALIGN sqi_cmd_dummy[6] = { DUMMY_BYTE, DUMMY_BYTE, DUMMY_BYTE, DUMMY_BYTE, DUMMY_BYTE, DUMMY_BYTE };

#define ms_SCALE (CPU_CLOCK_FREQUENCY / 2e3) // Millisecond scale
#define us_SCALE (CPU_CLOCK_FREQUENCY / 2e6) // Microsecond scale

static volatile bool binaryVar = false;
static uint32_t __attribute__((coherent, aligned(8))) flash_is_busy;

bool write_flip = false;

uint32_t j = 0;

uint32_t __attribute__((coherent, aligned(16))) write_configuration_register_data;
uint32_t __attribute__((coherent, aligned(16))) status_register_data;
uint32_t __attribute__((coherent, aligned(16))) protection_register_data;
uint32_t __attribute__((coherent, aligned(16))) configuration_register_data;

uint32_t flash_address = 0; // 0 - 128MB
uint16_t buffer_offset = 0; // 0 - 2048 or 2112
uint16_t block_number = 0; // 0 - 1023
uint16_t page_number = 0; // 0 - 65535
uint16_t new_page_number = 0; // 0 - 65535

uint8_t __attribute__((coherent, aligned(8))) PIC_flash_buffer[BLOCK_SIZE] = { 0 };
uint8_t __attribute__((coherent, aligned(8))) read_byte = 0;

uint16_t bad_block_total; 
uint32_t bad_block_array[MAX_BLOCKS];


/* 
 * Prototypes:
 */
void  SQI_TASK_Flash_Read_Status_Register( void *rx_data );


/*
 *  Functions: 
 */
uint32_t ReadCoreTimer( void )
{
    volatile uint32_t timer;
    asm volatile("mfc0 %0, $9" : "=r"(timer));
    return(timer);
}

void DelayUs( unsigned long int usDelay )
{
    register unsigned int startCntms = ReadCoreTimer();
    register unsigned int waitCntms = usDelay * us_SCALE;
    while( ReadCoreTimer() - startCntms < waitCntms );
}

void DelayMs( unsigned long int msDelay )
{
    register unsigned int startCntms = ReadCoreTimer();
    register unsigned int waitCntms = msDelay * ms_SCALE;
    while( ReadCoreTimer() - startCntms < waitCntms );
}

//
// Flash Variables: 
// ================
//
//      1. flash_address - Byte address: 0 to 128MB.
//      2. buffer_offset - Number of bytes from page start to flash_address. [ buffer_offset < page_zise ]
//      3. page_number   - Page number: 0 to 65535, 2048 Bytes per page. 
//      4. block_number  - block number: 0 to 1023. 128kB per block.

uint16_t SQI_TASK_Get_Page_Number( uint32_t F_address )
{
    int32_t result = 0;
    
    result = F_address / PAGE_SIZE;

    return (uint16_t)result;
}

uint16_t SQI_TASK_Get_Buffer_Offset( uint32_t F_address )
{
   uint16_t page_no;
   uint32_t result, temp;
   
    page_no = SQI_TASK_Get_Page_Number( F_address );
    temp = ( page_no * PAGE_SIZE );
    
    result = F_address - temp;
    
    return (uint16_t)result;
}

uint16_t SQI_TASK_Get_Block_Number( uint32_t F_address )
{
    uint16_t page_no; 
    uint32_t result;
    
    result = F_address / BLOCK_SIZE;
    
    if ( result >= MAX_BLOCKS )
    {
        result = 0;
    }
    
    return (uint16_t)result;
}

uint32_t SQI_TASK_Increment_Flash_Address( uint32_t F_address )
{
    uint32_t temp_result;
    
    F_address++;
    
    temp_result = FLASH_SIZE - F_address;
    
    if ( temp_result > 0 )
    {
        return F_address;
    }
    else
    {
        return 0;
    }   
}

void SQI_TASK_Update_Flash_Parameters(uint32_t F_address, uint16_t *offset, uint16_t *page_no, uint16_t *block_no)
{
    *offset = SQI_TASK_Get_Buffer_Offset( F_address );
    *page_no = SQI_TASK_Get_Page_Number( F_address );
    *block_no = SQI_TASK_Get_Block_Number( F_address );   
}

void SQI_TASK_Init_Global_Variables( void )
{
    for ( j=0; j<BLOCK_SIZE;j++ )
    {
        PIC_flash_buffer[j] = (uint8_t) j; // Temporary buffer for flash block storage while flash block is being erased.
    }
    for ( j=0; j<MAX_BLOCKS; j++ )
    {
        bad_block_array[j] = 0xAAAAAAAA;
    }
    
    flash_address = ( BLOCK_SIZE * 1 ) + ( PAGE_SIZE * 63 ) + 2046;  // = 0; // I just chose a random address for testing purposes. This should be Zero ( 0 )!
    
    block_number = SQI_TASK_Get_Block_Number( flash_address );
    page_number = SQI_TASK_Get_Page_Number( flash_address );
    buffer_offset = SQI_TASK_Get_Buffer_Offset( flash_address );    
}

static void SQI_TASK_EventHandler( uintptr_t context )
{
    sqi_taskData.xfer_done = true;
}

void SQI_TASK_Initialize( void )
{
    uint32_t i = 0;

    /* Fill the write buffer with linear data. */
    for (i = 0; i < BUFFER_SIZE; i++)
    {
        sqi_taskData.writeBuffer[i] = i;
        sqi_taskData.readBuffer[i] = 0xff;
    }
    
    /* Initialize the commands for the WINBOND W25N01GVxxxR Flash chip (64MByte): */
    sqi_cmd_flash_reset                      = W25N01GV_CMD_FLASH_RESET;
    sqi_cmd_jedec_id_read[0]                 = W25N01GV_CMD_JEDEC_ID_READ;
    sqi_cmd_read_status_register[0]          = W25N01GV_CMD_READ_STATUS_REG;
    sqi_cmd_write_status_register[0]         = W25N01GV_CMD_WRITE_STATUS_REG;
    sqi_cmd_write_enable                     = W25N01GV_CMD_WRITE_ENABLE;    
    sqi_cmd_write_disable                    = W25N01GV_CMD_WRITE_DISABLE;    
    sqi_cmd_block_erase[0]                   = W25N01GV_CMD_BLOCK_ERASE_128KB;
    sqi_cmd_program_data_load[0]             = W25N01GV_CMD_PROGRAM_DATA_LOAD;
    sqi_cmd_random_program_data_load[0]      = W25N01GV_CMD_RANDOM_PROGRAM_DATA_LOAD;
    sqi_cmd_quad_program_data_load[0]        = W25N01GV_CMD_QUAD_PROGRAM_DATA_LOAD;
    sqi_cmd_random_quad_program_data_load[0] = W25N01GV_CMD_RANDOM_QUAD_PROGRAM_DATA_LOAD;
    sqi_cmd_program_execute[0]               = W25N01GV_CMD_PROGRAM_EXECUTE;
    sqi_cmd_page_data_read[0]                = W25N01GV_CMD_PAGE_DATA_READ;
    sqi_cmd_read[0]                          = W25N01GV_CMD_READ;
    sqi_cmd_fast_read[0]                     = W25N01GV_CMD_FAST_READ;
    sqi_cmd_fast_read_dual_output[0]         = W25N01GV_CMD_FAST_READ_DUAL_OUTPUT;
    sqi_cmd_fast_read_quad_output[0]         = W25N01GV_CMD_FAST_READ_QUAD_OUTPUT;
    sqi_cmd_fast_read_dual_io[0]             = W25N01GV_CMD_FAST_READ_DUAL_IO;
    sqi_cmd_fast_read_quad_io[0]             = W25N01GV_CMD_FAST_READ_QUAD_IO;    

    SQI1_RegisterCallback(SQI_TASK_EventHandler, (uintptr_t)NULL);
    
    sqi_taskData.state = SQI_TASK_STATE_INIT;
}

void Start_Write_Enable_Timing( void )
{
    register unsigned int startCntTcks = ReadCoreTimer();
    start_write_enable = startCntTcks;                 
}

void Stop_Write_Enable_Timing( void )
{
    volatile register unsigned int stopCntTcks = ReadCoreTimer();
    stop_write_enable = stopCntTcks;
}

uint8_t SQI_TASK_Flash_is_Busy( void )
{
    volatile uint8_t retval = 0x01; 
    status_register_data = 0xFF;            
            
    SQI_TASK_Flash_Read_Status_Register( &status_register_data );
    
    retval &= (uint8_t) status_register_data;
    
    return retval; // Return 0 for Not Busy, 1 for Busy
}

void SQI_TASK_Flash_Write_Enable(void)
{
    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(1) | 
                                    SQI_BDCTRL_MODE_SINGLE_LANE | 
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DEASSERT | 
                                    SQI_BDCTRL_DESCEN); 
    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_write_enable);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[1]);
}

void SQI_TASK_Flash_Write_Disable(void)
{
    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(1) | 
                                    SQI_BDCTRL_DIR_WRITE |
                                    SQI_BDCTRL_PKTINTEN |   
                                    SQI_BDCTRL_LASTPKT | 
                                    SQI_BDCTRL_LASTBD | 
                                    SQI_BDCTRL_MODE_SINGLE_LANE | 
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DEASSERT | 
                                    SQI_BDCTRL_DESCEN);
    
    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_write_disable);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = 0x00000000; // (sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]); // 0x00000000;
    
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));    
}

void SQI_TASK_Flash_Reset(void)
{
    sqi_taskData.xfer_done = false;

    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(1) | 
                                    SQI_BDCTRL_DIR_WRITE |
                                    SQI_BDCTRL_PKTINTEN |   
                                    SQI_BDCTRL_LASTPKT | 
                                    SQI_BDCTRL_LASTBD |            
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DEASSERT |        
                                    SQI_BDCTRL_DESCEN | 
                                    SQI_BDCTRL_MODE_SINGLE_LANE);   

    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_flash_reset);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = 0x00000000;

    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));
    
    while( sqi_taskData.xfer_done == false ); 
    
    DelayUs(600);
    
    Nop();
}

void  SQI_TASK_Flash_Jedec_Id_Read( uint32_t *jedec_id)
{
    sqi_taskData.xfer_done = false;
    sqi_taskData.jedec_id = 0x00;
    sqi_cmd_jedec_id_read[1] = DUMMY_BYTE;
    
    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(2) | 
                                    SQI_BDCTRL_DIR_WRITE |
                                    SQI_BDCTRL_PKTINTEN | 
                                    SQI_BDCTRL_MODE_SINGLE_LANE |
                                    SQI_BDCTRL_SCHECK |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DESCEN);

    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_jedec_id_read);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiBufDesc[0]);

    sqiBufDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(3) | 
                                    SQI_BDCTRL_DIR_READ |
                                    SQI_BDCTRL_PKTINTEN |
                                    SQI_BDCTRL_LASTPKT | 
                                    SQI_BDCTRL_LASTBD |
                                    SQI_BDCTRL_MODE_SINGLE_LANE | 
                                    SQI_BDCTRL_DIR_READ |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DEASSERT |
                                    SQI_BDCTRL_SCHECK |
                                    SQI_BDCTRL_DESCEN);
    
    sqiBufDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(jedec_id);
    sqiBufDesc[0].bd_stat       = 0;
    sqiBufDesc[0].bd_nxtptr     = 0x00000000;

    // Initialize the root buffer descriptor
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));

    while(sqi_taskData.xfer_done == false);
    
    Nop();
}

void  SQI_TASK_Flash_Read_Configuration_Register( void *rx_data )
{
    sqi_taskData.xfer_done = false;
    sqi_cmd_read_status_register[1] = W25N_CONFIG_REG;
    
    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(2) | 
                                    SQI_BDCTRL_DIR_WRITE |
                                    SQI_BDCTRL_MODE_SINGLE_LANE |
                                    SQI_BDCTRL_PKTINTEN | 
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_SCHECK |
                                    SQI_BDCTRL_DESCEN); 

    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_read_status_register);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiBufDesc[0]);

    sqiBufDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(1) | 
                                    SQI_BDCTRL_PKTINTEN |
                                    SQI_BDCTRL_LASTPKT | 
                                    SQI_BDCTRL_LASTBD |
                                    SQI_BDCTRL_MODE_SINGLE_LANE | 
                                    SQI_BDCTRL_DIR_READ |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DEASSERT |
                                    SQI_BDCTRL_DESCEN); 

    sqiBufDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(rx_data);
    sqiBufDesc[0].bd_stat       = 0;
    sqiBufDesc[0].bd_nxtptr     = 0x00000000;

    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));

    while(sqi_taskData.xfer_done == false);
    
}

void  SQI_TASK_Flash_Read_Protection_Register( void *rx_data )
{
    sqi_taskData.xfer_done = false;
    sqi_cmd_read_status_register[1] = W25N_PROT_REG;
    
    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(2) | 
                                    SQI_BDCTRL_DIR_WRITE |
                                    SQI_BDCTRL_MODE_SINGLE_LANE |
                                    SQI_BDCTRL_PKTINTEN | 
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_SCHECK |
                                    SQI_BDCTRL_DESCEN); 

    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_read_status_register);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiBufDesc[0]);

    sqiBufDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(1) | 
                                    SQI_BDCTRL_PKTINTEN |
                                    SQI_BDCTRL_LASTPKT | 
                                    SQI_BDCTRL_LASTBD |
                                    SQI_BDCTRL_MODE_SINGLE_LANE | 
                                    SQI_BDCTRL_DIR_READ |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DEASSERT |
                                    SQI_BDCTRL_DESCEN); 

    sqiBufDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(rx_data);
    sqiBufDesc[0].bd_stat       = 0;
    sqiBufDesc[0].bd_nxtptr     = 0x00000000;

    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));

    while(sqi_taskData.xfer_done == false);
    
}

void  SQI_TASK_Flash_Read_Status_Register( void *rx_data )
{
    sqi_taskData.xfer_done = false;
    sqi_cmd_read_status_register[1] = W25N_STAT_REG;
    
    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(2) | 
                                    SQI_BDCTRL_DIR_WRITE |
                                    SQI_BDCTRL_MODE_SINGLE_LANE |
                                    SQI_BDCTRL_PKTINTEN | 
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_SCHECK |
                                    SQI_BDCTRL_DESCEN); 

    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_read_status_register);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiBufDesc[0]);

    sqiBufDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(1) | 
                                    SQI_BDCTRL_PKTINTEN |
                                    SQI_BDCTRL_LASTPKT | 
                                    SQI_BDCTRL_LASTBD |
                                    SQI_BDCTRL_MODE_SINGLE_LANE | 
                                    SQI_BDCTRL_DIR_READ |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DEASSERT |
                                    SQI_BDCTRL_DESCEN); 

    sqiBufDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(rx_data);
    sqiBufDesc[0].bd_stat       = 0;
    sqiBufDesc[0].bd_nxtptr     = 0x00000000;

    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));

    while(sqi_taskData.xfer_done == false);
    
}

void  SQI_TASK_Flash_Write_Status_Register( uint8_t rx_data )
{
    sqi_taskData.xfer_done = false;
    
    sqi_cmd_write_status_register[1] = (uint8_t) W25N_STAT_REG;
    sqi_cmd_write_status_register[2] = (uint8_t) rx_data;
    
    rx_data = 0;
    
    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(2)   | 
                                    SQI_BDCTRL_DIR_WRITE        |
                                    SQI_BDCTRL_MODE_SINGLE_LANE |                                     
                                    SQI_BDCTRL_SQICS_CS0        |
                                    SQI_BDCTRL_DESCEN); 
                                                
    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_write_status_register);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[1]);

    sqiCmdDesc[1].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(1)   | 
                                    SQI_BDCTRL_DIR_WRITE        |
                                    SQI_BDCTRL_MODE_SINGLE_LANE |                                     
                                    SQI_BDCTRL_PKTINTEN         | 
                                    SQI_BDCTRL_SQICS_CS0        | 
                                    SQI_BDCTRL_LASTBD           |
                                    SQI_BDCTRL_SCHECK           |
                                    SQI_BDCTRL_DEASSERT         |
                                    SQI_BDCTRL_DESCEN); 

    sqiCmdDesc[1].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_write_status_register[2]);
    sqiCmdDesc[1].bd_stat       = 0;
    sqiCmdDesc[1].bd_nxtptr     = 0x00000000;    
    
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));
    
    DelayUs(2);
}

void  SQI_TASK_Flash_Write_Configuration_Register( uint8_t rx_data )
{
    sqi_taskData.xfer_done = false;
    
    sqi_cmd_write_status_register[1] = (uint8_t) W25N_CONFIG_REG;
    sqi_cmd_write_status_register[2] = (uint8_t) rx_data;
    
    rx_data = 0;
    
    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(2)   | 
                                    SQI_BDCTRL_DIR_WRITE        |
                                    SQI_BDCTRL_MODE_SINGLE_LANE |                                     
                                    SQI_BDCTRL_SQICS_CS0        |
                                    SQI_BDCTRL_DESCEN); 
                                                
    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_write_status_register);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[1]);

    sqiCmdDesc[1].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(1)   | 
                                    SQI_BDCTRL_DIR_WRITE        |
                                    SQI_BDCTRL_MODE_SINGLE_LANE |                                     
                                    SQI_BDCTRL_PKTINTEN         | 
                                    SQI_BDCTRL_SQICS_CS0        | 
                                    SQI_BDCTRL_LASTBD           |
                                    SQI_BDCTRL_SCHECK           |
                                    SQI_BDCTRL_DEASSERT         |
                                    SQI_BDCTRL_DESCEN); 

    sqiCmdDesc[1].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_write_status_register[2]);
    sqiCmdDesc[1].bd_stat       = 0;
    sqiCmdDesc[1].bd_nxtptr     = 0x00000000;    
    
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));
    
    DelayUs(2);
}

void  SQI_TASK_Flash_Write_Protection_Register( uint8_t rx_data )
{
    sqi_taskData.xfer_done = false;
    
    sqi_cmd_write_status_register[1] = (uint8_t) W25N_PROT_REG;
    sqi_cmd_write_status_register[2] = (uint8_t) rx_data;
    
    rx_data = 0;
    
    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(2)   | 
                                    SQI_BDCTRL_DIR_WRITE        |
                                    SQI_BDCTRL_MODE_SINGLE_LANE |                                     
                                    SQI_BDCTRL_SQICS_CS0        |
                                    SQI_BDCTRL_DESCEN); 
                                                
    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_write_status_register);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[1]);

    sqiCmdDesc[1].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(1)   | 
                                    SQI_BDCTRL_DIR_WRITE        |
                                    SQI_BDCTRL_MODE_SINGLE_LANE |                                     
                                    SQI_BDCTRL_PKTINTEN         | 
                                    SQI_BDCTRL_SQICS_CS0        | 
                                    SQI_BDCTRL_LASTBD           |
                                    SQI_BDCTRL_SCHECK           |
                                    SQI_BDCTRL_DEASSERT         |
                                    SQI_BDCTRL_DESCEN); 

    sqiCmdDesc[1].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_write_status_register[2]);
    sqiCmdDesc[1].bd_stat       = 0;
    sqiCmdDesc[1].bd_nxtptr     = 0x00000000;    
    
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));
    
    DelayUs(2);
}

/*
 * The Page Data Read instruction will copy the data of the specified memory page into the 2,112-Byte Data Buffer of the Winbond flash.
 * Then the buffer can be read and written to with the other commands.
 * Important to note that the PIC32MZ will auto poll the flash to see when the transfer has completed (by executing the read command for status register 3).
 * See Harmony 3 SQI module setup for details (the two bytes are supposed to be reversed in the harmony input box -> 0xC005 instead of 0x05C0).
 * See the LA plots - it takes 51.12us to complete a transfer.
 */
void  SQI_TASK_Flash_Page_to_Flash_Buffer_Transfer( uint16_t page_no ) // page_number: 0 to 65535, each page is 2048 usable bytes
{
    /* 
     * Enable/Disable the flash ECC Engine to see the page transfer time difference between the two settings. 
     * Configuration_register_data = 0b00011000; // Enable(1) or Disable(0) the ECC Engine by manipulating bit 4. Bit 3 needs to be 1 (BUF=1) for Buffer Read Mode. 
     * SQI_TASK_Flash_Write_Status( W25N_CONFIG_REG, configuration_register_data );
     * ECC = Error Correction Coding:
     *      -> With ECC/Without ECC: Min 25us and Max 60us. In practice = 51.15us with ECC on and 22.58us with ECC off. */
    
    sqi_taskData.xfer_done = false;
    
    sqi_cmd_page_data_read[0] = W25N01GV_CMD_PAGE_DATA_READ;
    sqi_cmd_page_data_read[1] = DUMMY_BYTE;
    sqi_cmd_page_data_read[2] = (uint8_t) (page_no >> 8);
    sqi_cmd_page_data_read[3] = (uint8_t) page_no;
    
    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(4) |
                                    SQI_BDCTRL_DIR_WRITE |
                                    SQI_BDCTRL_MODE_SINGLE_LANE |
                                    SQI_BDCTRL_PKTINTEN |
                                    SQI_BDCTRL_SQICS_CS0 |  
                                    SQI_BDCTRL_LASTBD |
                                    SQI_BDCTRL_LASTPKT |
                                    SQI_BDCTRL_SCHECK |
                                    SQI_BDCTRL_DEASSERT |
                                    SQI_BDCTRL_DESCEN); 

    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_page_data_read);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = 0x00000000; //(sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[1]);
    
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));
    
    DelayUs(60); 
    
    Nop();
}
    
void SQI_TASK_Flash_Buffer_Read_Single_Output( void *rx_data, uint32_t rx_data_length, uint16_t address )
{
    sqi_taskData.xfer_done = false;
    
    sqi_cmd_read[0] = W25N01GV_CMD_READ; // 0x03
    sqi_cmd_read[1] = (uint8_t) (address >> 8);
    sqi_cmd_read[2] = (uint8_t) address;    
    sqi_cmd_read[3] = DUMMY_BYTE;
    
    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(4) | 
                                    SQI_BDCTRL_DIR_WRITE |
                                    SQI_BDCTRL_MODE_SINGLE_LANE |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DESCEN);

    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_read);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiBufDesc[0]);  

    
    sqiBufDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(rx_data_length) | 
                                    SQI_BDCTRL_PKTINTEN |
                                    SQI_BDCTRL_LASTPKT | 
                                    SQI_BDCTRL_LASTBD |
                                    SQI_BDCTRL_MODE_SINGLE_LANE | 
                                    SQI_BDCTRL_DIR_READ |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_SCHECK |
                                    SQI_BDCTRL_DEASSERT |
                                    SQI_BDCTRL_DESCEN); 

    sqiBufDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(rx_data);
    sqiBufDesc[0].bd_stat       = 0;
    sqiBufDesc[0].bd_nxtptr     = 0x00000000;

    // Initialize the root buffer descriptor
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));    
    
    DelayUs(370);
}

void SQI_TASK_Flash_Buffer_Read_Dual_Output( void *rx_data, uint32_t rx_data_length, uint16_t address )
{
    uint8_t *readBuffer = (uint8_t *)rx_data;

    sqi_taskData.xfer_done = false;
    flash_is_busy = 0x00000001;
    
    sqi_cmd_fast_read_dual_output[0] = W25N01GV_CMD_FAST_READ_DUAL_OUTPUT;
    sqi_cmd_fast_read_dual_output[1] = (uint8_t) (address>>8);
    sqi_cmd_fast_read_dual_output[2] = (uint8_t) (address>>0);    
    sqi_cmd_fast_read_dual_output[3] = DUMMY_BYTE;

    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(4) | 
                                    SQI_BDCTRL_MODE_SINGLE_LANE |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DESCEN);

    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_fast_read_dual_output);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiBufDesc[0]);  

    
    sqiBufDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(rx_data_length) | 
                                    SQI_BDCTRL_PKTINTEN |
                                    SQI_BDCTRL_LASTPKT | 
                                    SQI_BDCTRL_LASTBD |
                                    SQI_BDCTRL_MODE_DUAL_LANE | 
                                    SQI_BDCTRL_DIR_READ |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_SCHECK |
                                    SQI_BDCTRL_DEASSERT |
                                    SQI_BDCTRL_DESCEN); 

    sqiBufDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(readBuffer);
    sqiBufDesc[0].bd_stat       = 0;
    sqiBufDesc[0].bd_nxtptr     = 0x00000000;

    // Initialize the root buffer descriptor
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));    
    
    DelayUs(320);
    Nop();
}

void SQI_TASK_Flash_Buffer_Read_Quad_Output( void *rx_data, uint32_t rx_data_length, uint16_t address )
{
    uint8_t *readBuffer = (uint8_t *)rx_data;
    
    sqi_taskData.xfer_done = false;
    flash_is_busy = 0x00000001;
    
    sqi_cmd_fast_read_quad_output[0] = W25N01GV_CMD_FAST_READ_QUAD_OUTPUT;
    sqi_cmd_fast_read_quad_output[1] = (uint8_t) (address>>8);
    sqi_cmd_fast_read_quad_output[2] = (uint8_t) (address>>0);    
    sqi_cmd_fast_read_quad_output[3] = DUMMY_BYTE;

    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(4) | 
                                    SQI_BDCTRL_MODE_SINGLE_LANE |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DESCEN);

    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_fast_read_quad_output);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiBufDesc[0]);  

    
    sqiBufDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(rx_data_length) | 
                                    SQI_BDCTRL_PKTINTEN |
                                    SQI_BDCTRL_LASTPKT | 
                                    SQI_BDCTRL_LASTBD |
                                    SQI_BDCTRL_MODE_QUAD_LANE | 
                                    SQI_BDCTRL_DIR_READ |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_SCHECK |
                                    SQI_BDCTRL_DEASSERT |
                                    SQI_BDCTRL_DESCEN); 

    sqiBufDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(readBuffer);
    sqiBufDesc[0].bd_stat       = 0;
    sqiBufDesc[0].bd_nxtptr     = 0x00000000;

    // Initialize the root buffer descriptor
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));    
    
    DelayUs(120);
    Nop();
}

void SQI_TASK_Flash_Buffer_Read_Quad_Input_and_Quad_Output( void *rx_data, uint32_t rx_data_length, uint16_t address )
{
    uint8_t *readBuffer = (uint8_t *)rx_data;

    sqi_taskData.xfer_done = false;
    flash_is_busy = 0x00000001;
    
    sqi_cmd_fast_read_quad_output[0] = W25N01GV_CMD_FAST_READ_QUAD_OUTPUT;
    sqi_cmd_fast_read_quad_output[1] = (uint8_t) (address>>8);
    sqi_cmd_fast_read_quad_output[2] = (uint8_t) (address>>0);    
    sqi_cmd_fast_read_quad_output[3] = DUMMY_BYTE;
    sqi_cmd_fast_read_quad_output[4] = DUMMY_BYTE;

    /* For the Winbond flash, the command needs to be sent in Single mode and the rest in Quad mode */
    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(1) | 
                                    SQI_BDCTRL_MODE_SINGLE_LANE |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DESCEN);

    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_fast_read_quad_io[0]);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[1]);  

    sqiCmdDesc[1].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(4) | 
                                    SQI_BDCTRL_MODE_QUAD_LANE |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DESCEN);

    sqiCmdDesc[1].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_fast_read_quad_io[1]);
    sqiCmdDesc[1].bd_stat       = 0;
    sqiCmdDesc[1].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiBufDesc[0]);  
    
    sqiBufDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(rx_data_length) | 
                                    SQI_BDCTRL_PKTINTEN |
                                    SQI_BDCTRL_LASTPKT | 
                                    SQI_BDCTRL_LASTBD |
                                    SQI_BDCTRL_MODE_QUAD_LANE | 
                                    SQI_BDCTRL_DIR_READ |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_SCHECK |
                                    SQI_BDCTRL_DEASSERT |
                                    SQI_BDCTRL_DESCEN); 

    sqiBufDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(readBuffer);
    sqiBufDesc[0].bd_stat       = 0;
    sqiBufDesc[0].bd_nxtptr     = 0x00000000;

    // Initialize the root buffer descriptor
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));    
    
    DelayUs(120);
    Nop();
}

void SQI_TASK_Flash_Buffer_Write_PDL_Single( void *rx_data, uint32_t rx_data_length, uint16_t address ) // PDL == Program Data Load
{   
    uint8_t * writeBuffer = (uint8_t *) rx_data ;

    /* Send Flash Write Enable Command */
    SQI_TASK_Flash_Write_Enable();
    
    sqi_taskData.xfer_done = false;
    
    address &= 0x07ff; // Only addresses from 0x0000 to 0x07ff are valid. 0x7ff = 2047d
    
    sqi_cmd_program_data_load[0] = W25N01GV_CMD_PROGRAM_DATA_LOAD;
    sqi_cmd_program_data_load[1] = (uint8_t) (address>>8);
    sqi_cmd_program_data_load[2] = (uint8_t) (address); 
    
    /* For the Winbod flash, the command needs to be sent in Single mode and the rest in Quad mode */
    sqiCmdDesc[1].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(2) | 
                                    SQI_BDCTRL_MODE_SINGLE_LANE |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DESCEN);
    sqiCmdDesc[1].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_program_data_load[0]); // Program Data Load (0x02))
    sqiCmdDesc[1].bd_stat       = 0;
    sqiCmdDesc[1].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[2]);  

    sqiCmdDesc[2].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(1) | 
                                    SQI_BDCTRL_MODE_SINGLE_LANE |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DESCEN);
    sqiCmdDesc[2].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_program_data_load[2]);
    sqiCmdDesc[2].bd_stat       = 0;
    sqiCmdDesc[2].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiBufDesc[0]);  
    
    sqiBufDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(rx_data_length) | 
                                    SQI_BDCTRL_PKTINTEN |
                                    SQI_BDCTRL_LASTPKT | 
                                    SQI_BDCTRL_LASTBD |
                                    SQI_BDCTRL_MODE_SINGLE_LANE | 
                                    SQI_BDCTRL_DIR_WRITE |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_SCHECK |
                                    SQI_BDCTRL_DEASSERT |
                                    SQI_BDCTRL_DESCEN); 

    sqiBufDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(writeBuffer);
    sqiBufDesc[0].bd_stat       = 0;
    sqiBufDesc[0].bd_nxtptr     = 0x00000000;

    // Initialize the root buffer descriptor
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));    
    
    while(sqi_taskData.xfer_done == false);
    
    DelayUs(3); // This delay gives the PIC a chance to make CS High and correctly end the procedure.
    
    Nop();
}


void SQI_TASK_Flash_Buffer_Write_Random_PDL_Single( void *rx_data, uint32_t rx_data_length, uint16_t address ) // PDL == Program Data Load
{   
    uint8_t * writeBuffer = (uint8_t *) rx_data ;

    /* Send Flash Write Enable Command */
    SQI_TASK_Flash_Write_Enable();
    
    sqi_taskData.xfer_done = false;
    
    address &= 0x07ff; // Only addresses from 0x0000 to 0x07ff are valid. 0x7ff = 2047d
    
    sqi_cmd_random_program_data_load[0] = W25N01GV_CMD_RANDOM_PROGRAM_DATA_LOAD;
    sqi_cmd_random_program_data_load[1] = (uint8_t) (address>>8);
    sqi_cmd_random_program_data_load[2] = (uint8_t) (address); 
    
    /* For the Winbond flash, the command needs to be sent in Single mode and the rest in Quad mode */
    sqiCmdDesc[1].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(2) | 
                                    SQI_BDCTRL_MODE_SINGLE_LANE |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DESCEN);
    sqiCmdDesc[1].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_random_program_data_load[0]); // Program Data Load (0x02))
    sqiCmdDesc[1].bd_stat       = 0;
    sqiCmdDesc[1].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[2]);  

    sqiCmdDesc[2].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(1) | 
                                    SQI_BDCTRL_MODE_SINGLE_LANE |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DESCEN);
    sqiCmdDesc[2].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_random_program_data_load[2]);
    sqiCmdDesc[2].bd_stat       = 0;
    sqiCmdDesc[2].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiBufDesc[0]);  
    
    sqiBufDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(rx_data_length) | 
                                    SQI_BDCTRL_PKTINTEN |
                                    SQI_BDCTRL_LASTPKT | 
                                    SQI_BDCTRL_LASTBD |
                                    SQI_BDCTRL_MODE_SINGLE_LANE | 
                                    SQI_BDCTRL_DIR_WRITE |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_SCHECK |
                                    SQI_BDCTRL_DEASSERT |
                                    SQI_BDCTRL_DESCEN); 

    sqiBufDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(writeBuffer);
    sqiBufDesc[0].bd_stat       = 0;
    sqiBufDesc[0].bd_nxtptr     = 0x00000000;

    // Initialize the root buffer descriptor
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));    
    
    while(sqi_taskData.xfer_done == false);
    
    DelayUs(3); // This delay gives the PIC a chance to make CS High and correctly end the procedure.
    
    Nop();
}

void SQI_TASK_Flash_Buffer_Write_Random_PDL_Quad( void *rx_data, uint32_t rx_data_length, uint16_t address ) // PDL == Program Data Load
{   
    uint8_t * writeBuffer = (uint8_t *) rx_data;

    /* Send Flash Write Enable Command */
    SQI_TASK_Flash_Write_Enable();
    
    sqi_taskData.xfer_done = false;
    
    address &= 0x07ff; // Only addresses from 0x0000 to 0x07ff are valid. 0x7ff = 2047d
    
    sqi_cmd_random_quad_program_data_load[0] = W25N01GV_CMD_RANDOM_QUAD_PROGRAM_DATA_LOAD;
    sqi_cmd_random_quad_program_data_load[1] = (uint8_t) (address>>8);
    sqi_cmd_random_quad_program_data_load[2] = (uint8_t) (address); 
    
    /* For the Winbond flash, the command needs to be sent in Single mode and the rest in Quad mode */
    sqiCmdDesc[1].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(2) | 
                                    SQI_BDCTRL_MODE_SINGLE_LANE |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DESCEN);
    sqiCmdDesc[1].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_random_quad_program_data_load[0]); // Program Data Load (0x02))
    sqiCmdDesc[1].bd_stat       = 0;
    sqiCmdDesc[1].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[2]);  

    sqiCmdDesc[2].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(1) | 
                                    SQI_BDCTRL_MODE_SINGLE_LANE |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_DESCEN);
    sqiCmdDesc[2].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_random_quad_program_data_load[2]);
    sqiCmdDesc[2].bd_stat       = 0;
    sqiCmdDesc[2].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiBufDesc[0]);  
    
    sqiBufDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(rx_data_length) | 
                                    SQI_BDCTRL_PKTINTEN |
                                    SQI_BDCTRL_LASTPKT | 
                                    SQI_BDCTRL_LASTBD |
                                    SQI_BDCTRL_MODE_QUAD_LANE | 
                                    SQI_BDCTRL_DIR_WRITE |
                                    SQI_BDCTRL_SQICS_CS0 | 
                                    SQI_BDCTRL_SCHECK |
                                    SQI_BDCTRL_DEASSERT |
                                    SQI_BDCTRL_DESCEN); 

    sqiBufDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(writeBuffer);
    sqiBufDesc[0].bd_stat       = 0;
    sqiBufDesc[0].bd_nxtptr     = 0x00000000;

    // Initialize the root buffer descriptor
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));    
    
    while(sqi_taskData.xfer_done == false);
    
    DelayUs(3); // This delay gives the PIC a chance to make CS High and correctly end the procedure.
    
    Nop();
}

void  SQI_TASK_Write_Flash_Buffer_to_Flash_Memory( uint16_t address )
{
    sqi_taskData.xfer_done = false;

    /* Send Flash Write Enable Command */
    SQI_TASK_Flash_Write_Enable();
    
    sqi_cmd_program_execute[1] = (uint8_t) (DUMMY_BYTE);    
    sqi_cmd_program_execute[2] = (uint8_t) (address>>8);
    sqi_cmd_program_execute[3] = (uint8_t) (address>>0); 

    sqiCmdDesc[1].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(4)   | 
                                    SQI_BDCTRL_DIR_WRITE        |
                                    SQI_BDCTRL_MODE_SINGLE_LANE |                                     
                                    SQI_BDCTRL_PKTINTEN         | 
                                    SQI_BDCTRL_SQICS_CS0        |
                                    SQI_BDCTRL_LASTPKT          |
                                    SQI_BDCTRL_LASTBD           |
                                    SQI_BDCTRL_SCHECK           |
                                    SQI_BDCTRL_DEASSERT         |
                                    SQI_BDCTRL_DESCEN); 

    sqiCmdDesc[1].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_program_execute[0]);
    sqiCmdDesc[1].bd_stat       = 0;
    sqiCmdDesc[1].bd_nxtptr     = 0x00000000;    
    
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));

    DelayUs(700); // Spec says Min is 200us and max is 700us. LA says 84us. Need to verify this! 
    
    Nop();
}

 void  SQI_TASK_Flash_Block_Erase( uint16_t page_no ) // TBE = 2ms to 10ms. Found it to be +/- 535us for this particular flash chip (as seen on LA).
{
    sqi_taskData.xfer_done = false;

    sqi_cmd_block_erase[1] = (uint8_t) (DUMMY_BYTE);    
    sqi_cmd_block_erase[2] = (uint8_t) (page_no>>8);
    sqi_cmd_block_erase[3] = (uint8_t) (page_no>>0); 

    /* Send Flash Write Enable Command */
    SQI_TASK_Flash_Write_Enable();
    
    sqiCmdDesc[1].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(4)   | 
                                    SQI_BDCTRL_DIR_WRITE        |
                                    SQI_BDCTRL_MODE_SINGLE_LANE |                                     
                                    SQI_BDCTRL_PKTINTEN         | 
                                    SQI_BDCTRL_SQICS_CS0        | 
                                    SQI_BDCTRL_LASTPKT          |
                                    SQI_BDCTRL_LASTBD           |
                                    SQI_BDCTRL_SCHECK           |
                                    SQI_BDCTRL_DEASSERT         |
                                    SQI_BDCTRL_DESCEN); 

    sqiCmdDesc[1].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_block_erase[0]);
    sqiCmdDesc[1].bd_stat       = 0;
    sqiCmdDesc[1].bd_nxtptr     = 0x00000000;    
    
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));
    
    DelayUs(650); // MUST: This function does not work without this delay!
    
    Nop();
}


// =============================================================================================================================================================


void SQI_TASK_Read( void *rx_data, uint32_t rx_data_length, uint32_t address )
{
    uint32_t pendingBytes   = rx_data_length;
    uint8_t *readBuffer     = (uint8_t *)rx_data;
    uint32_t numBytes       = 0;
    uint32_t i              = 0;

    sqi_taskData.xfer_done = false;

    // Construct parameters to issue read command
//    sqi_cmd_hsr[1] = (0xff & (address>>16));
//    sqi_cmd_hsr[2] = (0xff & (address>>8));
//    sqi_cmd_hsr[3] = (0xff & (address>>0));    
//    sqi_cmd_hsr[4] = 0;

    sqiCmdDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(5) | SQI_BDCTRL_MODE_QUAD_LANE |
                                    SQI_BDCTRL_SQICS_CS0 | SQI_BDCTRL_DESCEN);

//    sqiCmdDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_hsr);
    sqiCmdDesc[0].bd_stat       = 0;
    sqiCmdDesc[0].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[1]);  

    sqiCmdDesc[1].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(2) | SQI_BDCTRL_MODE_QUAD_LANE |
                                    SQI_BDCTRL_SQICS_CS0 | SQI_BDCTRL_DESCEN);

    sqiCmdDesc[1].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_dummy);
    sqiCmdDesc[1].bd_stat       = 0;
    sqiCmdDesc[1].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiBufDesc[0]); 

    for (i = 0; (i < BUFF_DESC_NUMBER) && (pendingBytes > 0); i++)
    {
        if (pendingBytes > BUFFER_SIZE)
        {
            numBytes = BUFFER_SIZE;
        }
        else
        {
            numBytes = pendingBytes;
        }

        sqiBufDesc[i].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(numBytes) | SQI_BDCTRL_PKTINTEN |
                                        SQI_BDCTRL_MODE_QUAD_LANE | SQI_BDCTRL_DIR_READ |
                                        SQI_BDCTRL_SQICS_CS0 | SQI_BDCTRL_DESCEN);

        sqiBufDesc[i].bd_bufaddr    = (uint32_t *)KVA_TO_PA(readBuffer);
        sqiBufDesc[i].bd_stat       = 0;
        sqiBufDesc[i].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiBufDesc[i+1]);

        pendingBytes    -= numBytes;
        readBuffer      += numBytes;
    }

    /* The last descriptor must indicate the end of the descriptor list */
    sqiBufDesc[i-1].bd_ctrl         |= (SQI_BDCTRL_LASTPKT | SQI_BDCTRL_LASTBD |
                                        SQI_BDCTRL_DEASSERT);

    sqiBufDesc[i-1].bd_nxtptr       = 0x00000000;

    // Initialize the root buffer descriptor
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));
}

void SQI_TASK_PageWrite( void *tx_data, uint32_t address )
{
    sqi_taskData.xfer_done = false;

    Start_Write_Enable_Timing(); 
    SQI_TASK_Flash_Write_Enable();
    Stop_Write_Enable_Timing();
    
    // Construct parameters to issue page program command
//    sqi_cmd_pp[1] = (0xff & (address>>16));
//    sqi_cmd_pp[2] = (0xff & (address>>8));
//    sqi_cmd_pp[3] = (0xff & (address>>0));

    sqiCmdDesc[1].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(4) | SQI_BDCTRL_MODE_QUAD_LANE |
                                    SQI_BDCTRL_SQICS_CS0 | SQI_BDCTRL_DESCEN);

//    sqiCmdDesc[1].bd_bufaddr    = (uint32_t *)KVA_TO_PA(&sqi_cmd_pp);
    sqiCmdDesc[1].bd_stat       = 0;
    sqiCmdDesc[1].bd_nxtptr     = (sqi_dma_desc_t *)KVA_TO_PA(&sqiBufDesc[0]);

    sqiBufDesc[0].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(BUFFER_SIZE) | SQI_BDCTRL_PKTINTEN |
                                    SQI_BDCTRL_LASTPKT | SQI_BDCTRL_LASTBD |
                                    SQI_BDCTRL_MODE_QUAD_LANE | SQI_BDCTRL_SCHECK |
                                    SQI_BDCTRL_SQICS_CS0 | SQI_BDCTRL_DEASSERT |
                                    SQI_BDCTRL_DESCEN);

    sqiBufDesc[0].bd_bufaddr    = (uint32_t *)KVA_TO_PA(tx_data);
    sqiBufDesc[0].bd_stat       = 0;
    sqiBufDesc[0].bd_nxtptr     = 0x00000000;

    // Initialize the root buffer descriptor
    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));
}

void SQI_TASK_Erase( uint8_t *instruction, uint32_t length )
{
    sqi_taskData.xfer_done = false;

    SQI_TASK_Flash_Write_Enable();

    sqiCmdDesc[1].bd_ctrl       = ( SQI_BDCTRL_BUFFLEN_VAL(length) | SQI_BDCTRL_PKTINTEN |
                                    SQI_BDCTRL_LASTPKT | SQI_BDCTRL_LASTBD |
                                    SQI_BDCTRL_MODE_QUAD_LANE | SQI_BDCTRL_SCHECK |
                                    SQI_BDCTRL_SQICS_CS0 | SQI_BDCTRL_DEASSERT |
                                    SQI_BDCTRL_DESCEN);

    sqiCmdDesc[1].bd_bufaddr    = (uint32_t *)KVA_TO_PA(instruction);
    sqiCmdDesc[1].bd_stat       = 0;
    sqiCmdDesc[1].bd_nxtptr     = 0x00000000;

    SQI1_DMATransfer((sqi_dma_desc_t *)KVA_TO_PA(&sqiCmdDesc[0]));
}

void SQI_TASK_BlockErase( uint32_t address )
{
    sqi_cmd_block_erase[1] = DUMMY_BYTE;
    sqi_cmd_block_erase[2] = (0xff & (address>>8));
    sqi_cmd_block_erase[3] = (0xff & (address>>0));

    SQI_TASK_Erase(&sqi_cmd_block_erase[0], 4);
}

// =============================================================================================================================================================


void Start_All_Timing( void )
{
    register unsigned int startCntTcks = ReadCoreTimer();
    start_all = startCntTcks;                 
    //IO_1_Set();
}

void Stop_All_Timing( void )
{
    finished_all = true;
    //IO_1_Clear();
    volatile register unsigned int stopCntTcks = ReadCoreTimer();
    stop_all = stopCntTcks;
}

void Start_Erase_Timing( void )
{
    register unsigned int startCntTcks = ReadCoreTimer();
    start_erase = startCntTcks;                 
}

void Stop_Erase_Timing( void )
{
    volatile register unsigned int stopCntTcks = ReadCoreTimer();
    stop_erase = stopCntTcks;
}

void Start_Write_Timing( void )
{
    register unsigned int startCntTcks = ReadCoreTimer();
    start_write = startCntTcks;                 
}

void Stop_Write_Timing( void )
{
    volatile register unsigned int stopCntTcks = ReadCoreTimer();
    stop_write = stopCntTcks;
}

void Start_Verify_Timing( void )
{
    register unsigned int startCntTcks = ReadCoreTimer();
    start_verify = startCntTcks;                 
}

void Stop_Verify_Timing( void )
{
    volatile register unsigned int stopCntTcks = ReadCoreTimer();
    stop_verify = stopCntTcks;
}

void Wait_for_Switch_Press( void )
{
    /* Wait Until Switch pressed to avoid Unnecessary operations on SQI Flash after reset. */
    while( 1 )
    {
        if ( !binaryVar )
        {
            LED_RED_Set();
            LED_YEL_Clear();
            
            if (Reader1_Button_Get() == 0 )
            {
                binaryVar = true;
                LED_RED_Clear();
                sqi_taskData.state = SQI_TASK_STATE_RESET_FLASH;
                return;
            }
        }
        else
        {
            LED_YEL_Set();
            LED_RED_Clear();
            
            if (Reader2_Button_Get() == 0 )
            {                
                binaryVar = false;
                LED_RED_Clear();
                sqi_taskData.state = SQI_TASK_STATE_RESET_FLASH;
                return;
            }
        }
    }
}

uint8_t SQI_TASK_Flash_Any_Error( void )
{
    uint8_t temp;
    
    SQI_TASK_Flash_Read_Status_Register( &status_register_data );
    
    temp = ( status_register_data & STATUS_REG_ANY_ERROR_MASK );
    
    if ( temp == 0 || temp == 0b00010000 ) // If no errors  OR  ECC-0 Error  then return NO ERROR. ECC-0 = Corrected a 1 bit error.
    {
        temp = 0;
    }
    
    Nop();
    
    return temp;
}

uint8_t SQI_TASK_Flash_Erase_Failure( void )
{
    uint8_t result;
    
    SQI_TASK_Flash_Read_Status_Register( &status_register_data );
    
    result = (uint8_t)( status_register_data & STATUS_REG_ERASE_FAILURE_MASK );
    
    return result; 
}

uint8_t SQI_TASK_Flash_ECC_Errors( void )
{
    uint8_t result;
    
    SQI_TASK_Flash_Read_Status_Register( &status_register_data );
    
    result = (uint8_t) (status_register_data & STATUS_REG_ECC_ERROR_BIT_0_MASK & STATUS_REG_ECC_ERROR_BIT_1_MASK);
    
    return result;
}

void Format_sqi_taskData_Rx_and_Tx_Buffers( uint8_t rxData, uint8_t txData )
{
    uint16_t i;

    if ( rxData == 0xFE && txData == 0xEF )
    {
        for (i = 0; i < BUFFER_SIZE; i++)
        {
            sqi_taskData.readBuffer[i]  = i;
            sqi_taskData.writeBuffer[i] = i;
        }        
    }
    else
    {
        for (i = 0; i < BUFFER_SIZE; i++)
        {
            sqi_taskData.readBuffer[i]  = rxData;
            sqi_taskData.writeBuffer[i] = txData;
        }
    }
}            

void SQI_TASK_Change_sqi_taskData_RxTx_Buffer_Bytes( void )
{
    /* Write something different each time */
    if ( write_flip )
    {
        write_flip = false;
        /* Fill the write buffer with linear data. */
        Format_sqi_taskData_Rx_and_Tx_Buffers( 0xFE, 0xEF ); // 0xFE and 0xEF will fill buffers with 'i' -> { 0, 1, 2, 3, 4, ... }
    }
    else
    {
        write_flip = true;
        Format_sqi_taskData_Rx_and_Tx_Buffers( 0x18, 0x81 );
    }
}
/***************************************************************************************************************************************************************
 
  Function:
    void SQI_TASK_State_Machine ( void )

  Remarks:
    See prototype in SQI_Task.h

 ***************************************************************************************************************************************************************/

bool SQI_TASK_Erase_Entire_Flash( void )
{
    uint16_t block_no, page_no;

    for ( block_no=0; block_no<MAX_BLOCKS; block_no++ ) // 128kB per block, 1024 blocks, 64 pages per block, 65535 pages, 2kB per page.
    {
        page_no = block_no * PAGES_PER_BLOCK;

        /* Wait if the flash is still busy */
        while ( SQI_TASK_Flash_is_Busy() );

        SQI_TASK_Flash_Block_Erase( page_no );

        /* Do a status check on Status Register Bit S2 (Erase Failure) to see if erase was successful! */
        if ( SQI_TASK_Flash_Erase_Failure() )
        {
            Nop();
            return false;
        }    
    }   
        return true;
}

bool first_time_here = true;

void SQI_TASK_State_Machine ( void )
{
    /* 
     * Adjust the application's current state. 
     */
    switch ( sqi_taskData.state )
    {
        case SQI_TASK_STATE_INIT:
        {
            if ( first_time_here )
            {
                first_time_here = false;
                
                SQI_TASK_Flash_Reset();
                
                SQI_TASK_Init_Global_Variables();//Wait_for_Switch_Press();
            }
            
            //Start_All_Timing();
            sqi_taskData.state = SQI_TASK_WRITE_PROTECTION_REGISTER; //SQI_TASK_FLASH_PAGE_TO_FLASH_BUFFER_TRANSFER_II; // SQI_TASK_STATE_RESET_FLASH; // SQI_TASK_WRITE_FLASH_BUFFER_TO_FLASH_MEMORY; // SQI_TASK_STATE_RESET_FLASH;
            break;
        }
        
        case SQI_TASK_WRITE_PROTECTION_REGISTER:
        {
            protection_register_data = (uint8_t) 0b00000000;
            SQI_TASK_Flash_Write_Protection_Register( protection_register_data );
            
            sqi_taskData.state = SQI_TASK_STATE_READ_JEDEC_ID; // SQI_TASK_STATE_RESET_FLASH; // SQI_TASK_READ_STATUS_REGISTER_II;
            break;
        }
        
        case SQI_TASK_STATE_READ_JEDEC_ID:
        {         
            SQI_TASK_Flash_Jedec_Id_Read(&sqi_taskData.jedec_id);
            if (sqi_taskData.jedec_id != W25N01GV_JEDEC_ID) 
            {
                Buzzer_Clear();
                sqi_taskData.state = SQI_TASK_STATE_ERROR;
                break;
            }
                sqi_taskData.state = SQI_TASK_READ_PROTECTION_REGISTER; 
            break;
        }
        
        case SQI_TASK_READ_PROTECTION_REGISTER:
        {
            protection_register_data = 0x000000FF;           
            SQI_TASK_Flash_Read_Protection_Register( &protection_register_data );
            sqi_taskData.state = SQI_TASK_READ_CONFIGURATION_REGISTER; 
            break;
        }
        
        case SQI_TASK_READ_CONFIGURATION_REGISTER:
        {
            configuration_register_data = 0xFF;
            SQI_TASK_Flash_Read_Configuration_Register( &configuration_register_data );
            sqi_taskData.state = SQI_TASK_READ_STATUS_REGISTER;
            break;
        } 

        case SQI_TASK_READ_STATUS_REGISTER:
        {
            status_register_data = 0xFF;            
            SQI_TASK_Flash_Read_Status_Register( &status_register_data );
            sqi_taskData.state = SQI_TASK_STATE_ERASE_ENTIRE_FLASH; // SQI_TASK_STATE_ERASE_ENTIRE_FLASH;
            break;
        }
        
        case SQI_TASK_STATE_ERASE_ENTIRE_FLASH:
        {
            volatile bool flash_erase_status = false;
            
            flash_erase_status = SQI_TASK_Erase_Entire_Flash();
            
            if ( !flash_erase_status )
            {
                sqi_taskData.state = SQI_TASK_STATE_ERROR;
            }
            else
            {
                sqi_taskData.state = SQI_TASK_STATE_RESET_FLASH; 
            }
            
            break;
        }
        
        case SQI_TASK_STATE_RESET_FLASH:
        {            
            SQI_TASK_Flash_Reset();
            sqi_taskData.state = SQI_TASK_STATE_READ_JEDEC_ID_II;
            break;
        }
        
        case SQI_TASK_STATE_READ_JEDEC_ID_II:
        {         
            SQI_TASK_Flash_Jedec_Id_Read(&sqi_taskData.jedec_id);
            if (sqi_taskData.jedec_id != W25N01GV_JEDEC_ID) 
            {
                Buzzer_Clear();
                sqi_taskData.state = SQI_TASK_STATE_ERROR;
                break;
            }
                sqi_taskData.state = SQI_TASK_STATE_CHECK_FLASH_FOR_BAD_BLOCKS; 
            break;
        }
        
        case SQI_TASK_STATE_CHECK_FLASH_FOR_BAD_BLOCKS:
        {
            volatile uint32_t m = 0;            
            uint16_t block_no, page_no;
            
            bad_block_total = 0;
            
            for ( block_no=0; block_no<MAX_BLOCKS; block_no++ ) // 128kB per block, 1024 blocks, 64 pages per block, 65535 pages, 2kB per page.
            {
                page_no = block_no * PAGES_PER_BLOCK; // Pages 0 to 63 in Block 0, Pages 64 to 127 in Block 1, ...
                SQI_TASK_Flash_Page_to_Flash_Buffer_Transfer( page_no );
                
                /* Do a status check on Status Register Bit S2 (Erase Failure) to see if erase was successful! */
                if ( SQI_TASK_Flash_Any_Error() )
                {
                    Nop();
                    /* Reset Status Register */
                    SQI_TASK_Flash_Write_Status_Register( 0x00 );
                }
                
                /* Read the flash buffer */
                SQI_TASK_Flash_Buffer_Read_Single_Output( &read_byte, 1, 2049 ); // Only read 1 byte from address 0x0000 of each page.
                if ( read_byte != 0xff ) 
                {
                    bad_block_total++; 
                    if ( m < MAX_BLOCKS ) 
                    {
                        bad_block_array[m++] = page_no;
                    }
                }
                Nop();
            }
            sqi_taskData.state = SQI_TASK_READ_STATUS_REGISTER_II;
            
            break;
        }
        
        case SQI_TASK_READ_STATUS_REGISTER_II:
        {
            status_register_data = 0xFF;            
            SQI_TASK_Flash_Read_Status_Register( &status_register_data );
            sqi_taskData.state = SQI_TASK_READ_CONFIGURATION_REGISTER_II;
            break;
        }
        
        case SQI_TASK_READ_CONFIGURATION_REGISTER_II:
        {
            configuration_register_data = 0xFF;
            SQI_TASK_Flash_Read_Configuration_Register( &configuration_register_data );
            sqi_taskData.state = SQI_TASK_READ_PROTECTION_REGISTER_II; 
            break;
        }         
        
        case SQI_TASK_READ_PROTECTION_REGISTER_II:
        {
            protection_register_data = 0x000000FF;           
            SQI_TASK_Flash_Read_Protection_Register( &protection_register_data );
            
            sqi_taskData.state = SQI_TASK_FLASH_TEST;
            break;
        }        
        
        case SQI_TASK_FLASH_TEST:
        {
            static volatile unsigned int result = 0;
            static volatile float time_all = 0;
            
            Start_All_Timing();
            
            /* Reset Status Register */
            SQI_TASK_Flash_Write_Status_Register( 0x00 );
            SQI_TASK_Flash_Read_Status_Register( &status_register_data );
            
            //SQI_TASK_Flash_Block_Erase( page_number ); // time = 535us
           
            SQI_TASK_Flash_Page_to_Flash_Buffer_Transfer( page_number );
            
            Format_sqi_taskData_Rx_and_Tx_Buffers( 0x18, 0x81 );
            
            SQI_TASK_Flash_Buffer_Read_Quad_Output( (uint32_t *)&sqi_taskData.readBuffer[0], 1 , buffer_offset ); 
            //SQI_TASK_Flash_Buffer_Read_Quad_Output( (uint32_t *)&sqi_taskData.readBuffer[0], PAGE_SIZE, 0x0000 ); 
            
            SQI_TASK_Change_sqi_taskData_RxTx_Buffer_Bytes(); 
           
            SQI_TASK_Flash_Buffer_Write_Random_PDL_Quad( &sqi_taskData.writeBuffer[0], 1 , buffer_offset );
            //SQI_TASK_Flash_Buffer_Write_Random_PDL_Quad( &sqi_taskData.writeBuffer[0], PAGE_SIZE, 0x0000 );
            
            SQI_TASK_Write_Flash_Buffer_to_Flash_Memory( page_number );
            
            //
            // Now read the flash memory back to the flash buffer in order to verify the write action:
            //
            
            SQI_TASK_Flash_Page_to_Flash_Buffer_Transfer( page_number );

            Format_sqi_taskData_Rx_and_Tx_Buffers( 0x55, 0x01 );

            SQI_TASK_Flash_Buffer_Read_Quad_Output( (uint32_t *)&sqi_taskData.readBuffer[0], 5, 0x0000); 
            //SQI_TASK_Flash_Buffer_Read_Quad_Output( (uint32_t *)&sqi_taskData.readBuffer[0], PAGE_SIZE, 0x0000);
            
            Stop_All_Timing();
                    
            // All Time:
            result = stop_all - start_all;
            time_all = (float) (result / 100e3); // ms
            
            flash_address = SQI_TASK_Increment_Flash_Address( flash_address );
            SQI_TASK_Update_Flash_Parameters( flash_address, &buffer_offset, &page_number, &block_number );
                    
            sqi_taskData.state = SQI_TASK_READ_STATUS_REGISTER_II; // SQI_TASK_FLASH_BUFFER_READ_SINGLE_OUTPUT_II;
            break;
        }        

        
/*
 End of Application
 */
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
//        case SQI_TASK_FLASH_BUFFER_READ_SINGLE_OUTPUT_II:
//        {
//            Format_sqi_taskData_Rx_and_Tx_Buffers( 0, 0 );
//            
//            //buffer_offset = 0; // 0 to 2047 bytes
//            //SQI_TASK_Flash_Buffer_Read_Single_Output( (uint32_t *)&sqi_taskData.readBuffer[0],  (uint32_t) PAGE_SIZE, buffer_offset );
//            sqi_taskData.state = SQI_TASK_STATE_INIT; // SQI_TASK_FLASH_BUFFER_RANDOM_WRITE_SINGLE;
//            break;
//        }        
    
// -------------------------------------------------------------------------------------------------------------------------------------------------------------        
        
//        case SQI_TASK_FLASH_BUFFER_READ_DUAL_OUTPUT:
//        {
//            buffer_offset = 0x0000; // 0 to 2047 bytes
//            
//            SQI_TASK_Flash_Buffer_Read_Dual_Output( (uint32_t *)&sqi_taskData.readBuffer[0], (uint32_t) PAGE_SIZE, buffer_offset );
//            
//            sqi_taskData.state = SQI_TASK_FLASH_BUFFER_READ_QUAD_OUTPUT;
//            
//            break;
//        }
//        
//        case SQI_TASK_FLASH_BUFFER_READ_QUAD_OUTPUT:
//        {
//            buffer_offset = 0x0000; // 0 to 2047 bytes
//            
//            SQI_TASK_Flash_Buffer_Read_Quad_Output( (uint32_t *)&sqi_taskData.readBuffer[0], (uint32_t) PAGE_SIZE, buffer_offset );
//            
//            sqi_taskData.state = SQI_TASK_FLASH_BUFFER_READ_QUAD_INPUT_OUTPUT;
//            
//            break;
//        }
//
//        case SQI_TASK_FLASH_BUFFER_READ_QUAD_INPUT_OUTPUT:
//        {
//            buffer_offset = 0x0000; // 0 to 2047 bytes
//            
//            SQI_TASK_Flash_Buffer_Read_Quad_Input_and_Quad_Output( (uint32_t *)&sqi_taskData.readBuffer[0], (uint32_t) PAGE_SIZE, buffer_offset );
//            
//            sqi_taskData.state = SQI_TASK_FLASH_BUFFER_RANDOM_WRITE_SINGLE;
//            
//            break;
//        }
//        

//        case SQI_TASK_FLASH_BUFFER_READ_SINGLE_OUTPUT_II:
//        {
//            buffer_offset = 0x0000; // 0 to 2047 bytes
//            
//            SQI_TASK_Flash_Buffer_Read_Single_Output( (uint32_t *)&sqi_taskData.readBuffer[0], (uint32_t) PAGE_SIZE, buffer_offset );
//            
//            sqi_taskData.state = SQI_TASK_WRITE_FLASH_BUFFER_TO_FLASH_MEMORY;
//            
//            break;
//        }
        
//        case SQI_TASK_FLASH_BUFFER_READ_QUAD_OUTPUT_II:
//        {
//            buffer_offset = 0x0000;
//            
//            SQI_TASK_Flash_Buffer_Read_Quad_Output( (uint32_t *)&sqi_taskData.readBuffer[0], (uint32_t) PAGE_SIZE, buffer_offset );
//            
//            sqi_taskData.state = SQI_TASK_WRITE_FLASH_BUFFER_TO_FLASH_MEMORY;
//            
//            break;
//        }
        


        case SQI_TASK_STATE_ERASE_WAIT:
        {
                    
            if (sqi_taskData.xfer_done == true)
            {
                sector_index += 0; //SECTOR_SIZE;

                if (sector_index < BUFFER_SIZE)
                {
                    sqi_taskData.state = SQI_TASK_STATE_ERASE_ENTIRE_FLASH;
                }
                else
                {
                    Stop_Erase_Timing();
                    Start_Write_Timing();
                    sqi_taskData.state = SQI_TASK_STATE_INIT; //SQI_TASK_STATE_WRITE_MEMORY;
                }
            }
            break;
        }
        
        case SQI_TASK_STATE_WRITE_WAIT:
        {
            if (sqi_taskData.xfer_done == true)
            {
                write_index += BUFFER_SIZE;
                if (write_index < BUFFER_SIZE)
                {
                    sqi_taskData.state = SQI_TASK_STATE_WRITE_MEMORY;
                }
                else
                {
                    Stop_Write_Timing();
                    Start_Verify_Timing();
                    sqi_taskData.state = SQI_TASK_STATE_READ_MEMORY;
                }
            }
            break;
        }

        case SQI_TASK_STATE_READ_MEMORY:
        {            
            SQI_TASK_Read((uint32_t *)&sqi_taskData.readBuffer[0], BUFFER_SIZE, MEM_START_ADDRESS);
            sqi_taskData.state = SQI_TASK_STATE_READ_WAIT;
            break;
        }

        case SQI_TASK_STATE_READ_WAIT:
        {
            if (sqi_taskData.xfer_done == true)
            {
                sqi_taskData.state = SQI_TASK_STATE_VERIFY_DATA;
            }
            break;
        }

        case SQI_TASK_STATE_VERIFY_DATA:
        {
            if (!memcmp(sqi_taskData.writeBuffer, sqi_taskData.readBuffer, BUFFER_SIZE))
            {
                Stop_Verify_Timing();
                Stop_All_Timing();
                sqi_taskData.state = SQI_TASK_STATE_SUCCESS;
            }
            else
            {
                sqi_taskData.state = SQI_TASK_STATE_ERROR;
            }

            break;
        }

        case SQI_TASK_STATE_SUCCESS:
        {            
            sqi_taskData.state = SQI_TASK_STATE_INIT;
            break;
        }

        case SQI_TASK_STATE_ERROR:
        default:
        {
            sqi_taskData.state = SQI_TASK_STATE_INIT;
            
            Nop();
            
            break;
        }
    }    
}


/***************************************************************************************************************************************************************
 End of File
 */
