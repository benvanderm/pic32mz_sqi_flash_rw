/*******************************************************************************
 
  Author: 
    Bennie van der Merwe
 
  Date:
    October, 2021
 
   File Name:
    sqi_task.h

  Summary:
    - This file contains source code, modified from Microchip example code,  
    for the PIC32MZ2048EFM144's SQI module command interface. 
    - Communication is specifically with the Winbond W25N01GVxxxR Flash Device.
 
 *******************************************************************************/

#ifndef _SQI_TASK_H
#define _SQI_TASK_H

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

//#include <stdint.h>
//#include <stdbool.h>
//#include <stddef.h>
//#include <stdlib.h>
#include <string.h>

//#include "configuration.h"
#include "definitions.h"
//#include "helper_functions.h"
#include "config/pic32mz_ef_sk/peripheral/sqi/plib_sqi1.h"

// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

extern "C" {

#endif
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Type Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application states

  Summary:
    Application states enumeration

  Description:
    This enumeration defines the valid application states.  These states
    determine the behavior of the application at various times.
*/


//#define SECTOR_SIZE                (2048U)
/* Erase, Write and Read 80KBytes of memory */
//#define SECTORS_TO_EWR             (1U)
    
#define PAGE_SIZE                  (2048U)
#define MAX_PAGE_NUMBER            (65535U)
#define PAGES_PER_BLOCK            (64U)
#define MAX_BLOCKS                 (1024U)
#define BUFFER_SIZE                (2048U)
#define BLOCK_SIZE                 (131072U) // 64 * 2kB = 128kB
#define MAX_BLOCKS                 (1024U)
#define BUFF_DESC_NUMBER           (BLOCK_SIZE / BUFFER_SIZE) // == 128kByte / 2kByte = 64
#define FLASH_SIZE                 (134217728U) // 128MByte  
    
#define MEM_START_ADDRESS          (0x0U)

#define SST26VF032B_JEDEC_ID       (0xBF4226BFUL)
#define SST26VF064B_JEDEC_ID       (0xBF4326BFUL)
    
#define W25N01GV_JEDEC_ID          (0x0021AAEFUL)   
#define W25N02GV_JEDEC_ID          (0x00EFAB21UL)  

#define CMD_DESC_NUMBER             5

#define DUMMY_BYTE                  0x0

#define SUCCESS_LED_ON              LED3_Set
#define SUCCESS_LED_OFF             LED3_Clear
#define SUCCESS_LED_TOGGLE          LED3_Toggle

#define SWITCH_LED_ON               LED1_Set
#define SWITCH_LED_OFF              LED1_Clear
#define SWITCH_LED_TOGGLE           LED1_Toggle

//#define SWITCH_PRESSED              (bool)!SWITCH_3_Get()
    
  
#define LED1_On() LED1_Clear()
#define LED1_Off() LED1_Set()
#define LED2_On() LED2_Clear()
#define LED2_Off() LED2_Set()
#define LED3_On() LED3_Clear()
#define LED3_Off() LED3_Set()


// Status Registers:
#define W25N_PROT_REG       0xA0
#define W25N_CONFIG_REG     0xB0
#define W25N_STAT_REG       0xC0

/* Status Register Bit Masks */
#define STATUS_REG_BUSY_MASK                0b00000001    
#define STATUS_REG_WRITE_ENABLE_MASK        0b00000010
#define STATUS_REG_ERASE_FAILURE_MASK       0b00000100
#define STATUS_REG_PROGRAM_FAILURE_MASK     0b00001000
#define STATUS_REG_ECC_ERROR_BIT_0_MASK     0b00010000
#define STATUS_REG_ECC_ERROR_BIT_1_MASK     0b00100000
#define STATUS_REG_BBM_LUT_FULL_MASK        0b01000000
#define STATUS_REG_ANY_ERROR_MASK           0b01111100
    
volatile bool finished_all;

volatile unsigned int start_erase;
volatile unsigned int start_write_enable;
volatile unsigned int start_write;
volatile unsigned int start_verify;
volatile unsigned int start_all;

volatile unsigned int stop_erase;
volatile unsigned int stop_write_enable;
volatile unsigned int stop_write;
volatile unsigned int stop_verify;
volatile unsigned int stop_all;

typedef enum
{
    /* The app mounts the disk */
    SQI_TASK_STATE_INIT = 0,
         
    SQI_TASK_FLASH_TEST,
            
    /* Check flash for bad blocks */
    SQI_TASK_STATE_CHECK_FLASH_FOR_BAD_BLOCKS,
            
    /* Reset Flash*/
    SQI_TASK_STATE_RESET_FLASH,
    SQI_TASK_STATE_RESET_FLASH_II,
    SQI_TASK_STATE_RESET_FLASH_III,
    /* Enable Quad IO Mode*/
    SQI_TASK_STATE_ENABLE_QUAD_IO,

    /* Unlock Flash*/
    SQI_TASK_STATE_UNLOCK_FLASH,

    /* Read JEDEC ID*/
    SQI_TASK_STATE_READ_JEDEC_ID,
    SQI_TASK_STATE_READ_JEDEC_ID_II,
            
    /* Write Flash Protection Register */
    SQI_TASK_WRITE_PROTECTION_REGISTER,

    /* Write Flash Configuration Register */        
    SQI_TASK_WRITE_CONFIGURATION_REGISTER,
            
    /* Write Flash Status Register */  
    SQI_TASK_WRITE_STATUS_REGISTER,
            
    /* Read Flash Status Register */        
    SQI_TASK_READ_STATUS_REGISTER,
    SQI_TASK_READ_STATUS_REGISTER_II,
            
    /* Read Flash Protection Register */        
    SQI_TASK_READ_PROTECTION_REGISTER,
    SQI_TASK_READ_PROTECTION_REGISTER_II,
            
    /* Read Flash Configuration Register */        
    SQI_TASK_READ_CONFIGURATION_REGISTER,
    SQI_TASK_READ_CONFIGURATION_REGISTER_II,       
            
    /* Transfer a page (2112 bytes) from flash 
     * memory to the flash buffer */
    SQI_TASK_FLASH_PAGE_TO_FLASH_BUFFER_TRANSFER,
    SQI_TASK_FLASH_PAGE_TO_FLASH_BUFFER_TRANSFER_II,
            
    /* Read the Flash Buffer */
    SQI_TASK_FLASH_BUFFER_READ_SINGLE_OUTPUT,
    SQI_TASK_FLASH_BUFFER_READ_SINGLE_OUTPUT_II,
            
    /* Fast Read Dual Output */ 
    SQI_TASK_FLASH_BUFFER_READ_DUAL_OUTPUT,
            
    /* Fast Read Quad Output */        
    SQI_TASK_FLASH_BUFFER_READ_QUAD_OUTPUT,
    SQI_TASK_FLASH_BUFFER_READ_QUAD_OUTPUT_II,
            
    /* Fast Read Quad Input AND Quad Output */
    SQI_TASK_FLASH_BUFFER_READ_QUAD_INPUT_OUTPUT,
                
    /* Erase Flash */
    SQI_TASK_STATE_ERASE_ENTIRE_FLASH,
    SQI_TASK_STATE_ERASE_FLASH_BLOCK,
    SQI_TASK_STATE_ERASE_FLASH_BLOCK_II,
        
    /* Erase Wait */
    SQI_TASK_STATE_ERASE_WAIT,

    /* Write to Memory */
    SQI_TASK_STATE_WRITE_MEMORY,
            
    /* Write to Flash buffer */
    SQI_TASK_FLASH_BUFFER_WRITE_SINGLE,             // Will Reset the unused data bytes in the flash buffer to 0xFF.
    SQI_TASK_FLASH_BUFFER_RANDOM_WRITE_SINGLE,      // Will NOT Reset the unused data bytes in the flash buffer.
            
    /* Write to Flash buffer */
    SQI_TASK_FLASH_BUFFER_WRITE_QUAD,
            
    /* Write Flash Buffer to Flash Memory */
    SQI_TASK_WRITE_FLASH_BUFFER_TO_FLASH_MEMORY,
            
    /* Write Wait */
    SQI_TASK_STATE_WRITE_WAIT,

    /* Read From Memory */
    SQI_TASK_STATE_READ_MEMORY,

    /* Read Wait */
    SQI_TASK_STATE_READ_WAIT,

    /* Verify Data Read */
    SQI_TASK_STATE_VERIFY_DATA,

    /* The app idles */
    SQI_TASK_STATE_SUCCESS,
            
    /* Write Page Number into first two bytes of each flash block, then read it back again to verify */
    SQI_TASK_STATE_TEST_FLASH_BLOCKS,

    /* An app error has occurred */
    SQI_TASK_STATE_ERROR

} SQI_TASK_STATES;
    
//typedef enum
//{
//    /* Application's state machine's initial state. */
//    SQI_TASK_STATE_INIT=0,
//    SQI_TASK_STATE_SERVICE_TASKS,
//    /* TODO: Define states used by the application state machine. */
//
//} SQI_TASK_STATES;


// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    Application strings and buffers are be defined outside this structure.
 */

typedef struct
{
    /* Application's current state */
    SQI_TASK_STATES state;

    /* Application transfer status */
    volatile bool xfer_done;

    /* Jedec-ID*/
    uint32_t jedec_id;

    /* Read Buffer */
    uint8_t __attribute__((coherent)) readBuffer[BUFFER_SIZE];

    /* Write Buffer*/
    uint8_t __attribute__((coherent)) writeBuffer[BUFFER_SIZE];

} SQI_TASK_DATA;

//typedef struct
//{
//    /* The application's current state */
//    SQI_TASK_STATES state;
//
//    /* TODO: Define any additional data used by the application. */
//
//} SQI_TASK_DATA;


/* SST26 Command set

  Summary:
    Enumeration listing the SST26VF commands.

  Description:
    This enumeration defines the commands used to interact with the SST26VF
    series of devices.

  Remarks:
    None
*/
//
//typedef enum
//{
//    /* Reset enable command. */
//    SST26_CMD_FLASH_RESET_ENABLE = 0x66,
//
//    /* Command to reset the flash. */
//    SST26_CMD_FLASH_RESET        = 0x99,
//
//    /* Command to Enable QUAD IO */
//    SST26_CMD_ENABLE_QUAD_IO     = 0x38,
//
//    /* Command to Reset QUAD IO */
//    SST26_CMD_RESET_QUAD_IO      = 0xFF,
//
//    /* Command to read JEDEC-ID of the flash device. */
//    SST26_CMD_JEDEC_ID_READ      = 0x9F,
//
//    /* QUAD Command to read JEDEC-ID of the flash device. */
//    SST26_CMD_QUAD_JEDEC_ID_READ = 0xAF,
//
//    /* Command to perfrom High Speed Read */
//    SST26_CMD_HIGH_SPEED_READ    = 0x0B,
//
//    /* Write enable command. */
//    SST26_CMD_WRITE_ENABLE       = 0x06,
//
//    /* Page Program command. */
//    SST26_CMD_PAGE_PROGRAM       = 0x02,
//
//    /* Command to read the Flash status register. */
//    SST26_CMD_READ_STATUS_REG    = 0x05,
//
//    /* Command to perform sector erase */
//    SST26_CMD_SECTOR_ERASE       = 0x20,
//
//    /* Command to perform Bulk erase */
//    SST26_CMD_BULK_ERASE_64K     = 0xD8,
//
//    /* Command to perform Chip erase */
//    SST26_CMD_CHIP_ERASE         = 0xC7,
//
//    /* Command to unlock the flash device. */
//    SST26_CMD_UNPROTECT_GLOBAL   = 0x98
//
//} SST26_CMD;


typedef enum
{
    /* Command to reset the flash. */
    W25N01GV_CMD_FLASH_RESET                    = 0xFF,

    /* Command to read JEDEC-ID of the flash device. */
    W25N01GV_CMD_JEDEC_ID_READ                  = 0x9F,

    /* Command to read the Flash status register. */
    W25N01GV_CMD_READ_STATUS_REG                = 0x0F,

    /* Command to write the Flash status register. */
    W25N01GV_CMD_WRITE_STATUS_REG               = 0x1F,

    /* Write enable command. */
    W25N01GV_CMD_WRITE_ENABLE                   = 0x06,
            
    /* Write disable command. */
    W25N01GV_CMD_WRITE_DISABLE                  = 0x04,
            
    /* Command to perform Block erase */
    W25N01GV_CMD_BLOCK_ERASE_128KB              = 0xD8,

    /* Program Data Load command. */
    W25N01GV_CMD_PROGRAM_DATA_LOAD              = 0x02, // Reset unused bytes to 0xFF           

    /* Random Program Data Load command. */
    W25N01GV_CMD_RANDOM_PROGRAM_DATA_LOAD       = 0x84,  
            
    /* Quad Program Data Load command. */
    W25N01GV_CMD_QUAD_PROGRAM_DATA_LOAD         = 0x32, // Reset unused bytes to 0xFF
            
    /* Random Quad Program Data Load command. */
    W25N01GV_CMD_RANDOM_QUAD_PROGRAM_DATA_LOAD  = 0x34,  
            
    /* Program Execute command. */
    W25N01GV_CMD_PROGRAM_EXECUTE                = 0x10,            
            
    /* Command to perform a flash Page Data Transfer to flash Buffer */
    W25N01GV_CMD_PAGE_DATA_READ                 = 0x13,            
            
    /* Command to perform a Read */
    W25N01GV_CMD_READ                           = 0x03,            
            
    /* Command to perform Fast Read */
    W25N01GV_CMD_FAST_READ                      = 0x0B,

    /* Command to perform Fast Read Dual Output */
    W25N01GV_CMD_FAST_READ_DUAL_OUTPUT          = 0x3B,
            
    /* Command to perform Fast Read Quad Output */
    W25N01GV_CMD_FAST_READ_QUAD_OUTPUT          = 0x6B,            
            
    /* Command to perform Fast Read Dual I/O */
    W25N01GV_CMD_FAST_READ_DUAL_IO              = 0xBB,

    /* Command to perform Fast Read Quad I/O */
    W25N01GV_CMD_FAST_READ_QUAD_IO              = 0xEB, 
            
} W25N01GV_CMD;


// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Routines
// *****************************************************************************
// *****************************************************************************
/* These routines are called by drivers when certain events occur.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void SQI_TASK_Initialize ( void )

  Summary:
     MPLAB Harmony application initialization routine.

  Description:
    This function initializes the Harmony application.  It places the
    application in its initial state and prepares it to run so that its
    SQI_TASK_State_Machine function can be called.

  Precondition:
    All other system initialization routines should be called before calling
    this routine (in "SYS_Initialize").

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    SQI_TASK_Initialize();
    </code>

  Remarks:
    This routine must be called from the SYS_Initialize function.
*/

void SQI_TASK_Initialize ( void );


/*******************************************************************************
  Function:
    void SQI_TASK_State_Machine ( void )

  Summary:
    MPLAB Harmony Demo application tasks function

  Description:
    This routine is the Harmony Demo application's tasks function.  It
    defines the application's state machine and core logic.

  Precondition:
    The system and application initialization ("SYS_Initialize") should be
    called before calling this.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    SQI_TASK_State_Machine();
    </code>

  Remarks:
    This routine must be called from SYS_Tasks() routine.
 */

void SQI_TASK_State_Machine( void );

//DOM-IGNORE-BEGIN
#ifdef __cplusplus
}
#endif
//DOM-IGNORE-END

#endif /* _SQI_TASK_H */

/*******************************************************************************
 End of File
 */

